package xmongo

type CreatingHook interface {
	Creating() error
}

type CreatedHook interface {
	Created() error
}

type UpdatingHook interface {
	Updating() error
}

type UpdatedHook interface {
	Updated() error
}

type SavingHook interface {
	Saving() error
}

type DeletingHook interface {
	Deleting() error
}

type DeletedHook interface {
	Deleted() error
}

func callToBeforeCreateHooks(model Model) error {
	if hook, ok := model.(CreatingHook); ok {
		if err := hook.Creating(); err != nil {
			return err
		}
	}

	if hook, ok := model.(SavingHook); ok {
		if err := hook.Saving(); err != nil {
			return err
		}
	}

	return nil
}

func callToAfterCreateHooks(model Model) error {
	if hook, ok := model.(CreatedHook); ok {
		if err := hook.Created(); err != nil {
			return err
		}
	}

	return nil
}

func callToBeforeUpdateHooks(model Model) error {
	if hook, ok := model.(UpdatingHook); ok {
		if err := hook.Updating(); err != nil {
			return err
		}
	}

	if hook, ok := model.(SavingHook); ok {
		if err := hook.Saving(); err != nil {
			return err
		}
	}

	return nil
}

func callToAfterUpdateHooks(model Model) error {
	if hook, ok := model.(UpdatedHook); ok {
		if err := hook.Updated(); err != nil {
			return err
		}
	}

	return nil
}

func callToBeforeDeleteHooks(model Model) error {
	if hook, ok := model.(DeletingHook); ok {
		if err := hook.Deleting(); err != nil {
			return err
		}
	}

	if hook, ok := model.(SavingHook); ok {
		if err := hook.Saving(); err != nil {
			return err
		}
	}

	return nil
}

func callToAfterDeleteHooks(model Model) error {
	if hook, ok := model.(DeletedHook); ok {
		if err := hook.Deleted(); err != nil {
			return err
		}
	}

	return nil
}
