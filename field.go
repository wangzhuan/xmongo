package xmongo

import (
	"time"

	"gitlab.com/wangzhuan/github.com.globalsign.mgo/bson"
)

const (
	ID           = "_id"
	UpdatedAt    = "updated_at"
	CreatedAt    = "created_at"
	DeletedField = "deleted"
	Deleted      = 1
	NotDel       = 0
	Set          = "$set"
)

func (f *IDField) PrepareID(id interface{}) (interface{}, error) {
	if idStr, ok := id.(string); ok {
		return bson.ObjectIdHex(idStr), nil
	}

	return id, nil
}

func (f *IDField) GetID() interface{} {
	return f.ID
}

func (f *IDField) SetID(id interface{}) {
	if id == nil {
		f.ID = bson.NewObjectId()
		return
	}

	if idStr, ok := id.(string); ok {
		f.ID = bson.ObjectIdHex(idStr)
	}
}

type DelField struct {
	// 1为删除 0为正常
	Deleted int `bson:"deleted" json:"-"`
}

type DefaultModel struct {
	IDField    `bson:",inline"`
	DateFields `bson:",inline"`
}

type IDField struct {
	ID bson.ObjectId `json:"id" bson:"_id,omitempty"`
}

type DateFields struct {
	CreatedAt time.Time `bson:"created_at" json:"createdAt" time_format:"2006-01-02 15:04:05"`
	UpdatedAt time.Time `bson:"updated_at" json:"updatedAt" time_format:"2006-01-02 15:04:05"`
}

func (f *DateFields) Creating() error {
	f.CreatedAt = time.Now()
	return nil
}

func (f *DateFields) Saving() error {
	f.UpdatedAt = time.Now()
	return nil
}

func (f *DelField) Deleting() error {
	f.Deleted = Deleted
	return nil
}
