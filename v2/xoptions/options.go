package xoptions

import (
	"errors"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type IMongoOption interface {
	GetSplitValues() map[string]interface{}
}

type FindOptions struct {
	options.FindOptions
	SplitValues      map[string]interface{}
	PrimaryPreferred *bool //数据同步时会涉及读主库
}

func (c *FindOptions) SetPrimaryPreferred(v bool) *FindOptions {
	c.PrimaryPreferred = &v
	return c
}

func (c *FindOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type CountOptions struct {
	options.CountOptions
	SplitValues map[string]interface{}
}

func (c *CountOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

func GetSplitValues(opts ...IMongoOption) map[string]interface{} {
	if len(opts) < 1 {
		return nil
	}
	for _, item := range opts {
		sv := item.GetSplitValues()
		if sv != nil {
			return sv
		}
	}
	return nil
}

type FindOneOptions struct {
	options.FindOneOptions
	SplitValues      map[string]interface{}
	PrimaryPreferred *bool
}

func (c *FindOneOptions) SetPrimaryPreferred(v bool) *FindOneOptions {
	c.PrimaryPreferred = &v
	return c
}

func (c *FindOneOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type UpdateOptions struct {
	options.UpdateOptions
	SplitValues map[string]interface{}
}

func (c *UpdateOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type DeleteOptions struct {
	options.DeleteOptions
	SplitValues map[string]interface{}
}

func (c *DeleteOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type FindOneAndUpdateOptions struct {
	options.FindOneAndUpdateOptions
	SplitValues map[string]interface{}
}

func (c *FindOneAndUpdateOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type FindOneAndDeleteOptions struct {
	options.FindOneAndDeleteOptions
	SplitValues map[string]interface{}
}

func (c *FindOneAndDeleteOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type DatabaseOptions struct {
	options.DatabaseOptions
}

type CollectionOptions struct {
	options.CollectionOptions
}

type InsertOneOptions struct {
	options.InsertOneOptions
	SplitValues map[string]interface{}
}

func (c *InsertOneOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type InsertManyOptions struct {
	options.InsertManyOptions
	SplitValues map[string]interface{}
}

func (c *InsertManyOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type DistinctOptions struct {
	options.DistinctOptions
	SplitValues map[string]interface{}
}

func (c *DistinctOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type BulkWriteOptions struct {
	options.BulkWriteOptions
	SplitValues map[string]interface{}
}

func (c *BulkWriteOptions) GetSplitValues() map[string]interface{} {
	return c.SplitValues
}

type DBOptions struct {
	DBSplitInfo *DBSplitInfo
	DBInfo      *DBInfo
}

type DBSplitInfo struct {
	SplitKeys SplitKeys // 需要的分库分表字段
	DBFunc    DBFunc    // 外部如何使用用于分库分表的 { key : value, ... }
	ColFunc   ColFunc   // 外部如何使用用于分库分表的 { key : value, ... }
}

func (d *DBSplitInfo) Validate() error {
	if d.DBFunc == nil {
		return errors.New("DBFunc is nil")
	}
	if d.ColFunc == nil {
		return errors.New("ColFunc is nil")
	}
	if d.SplitKeys == nil {
		return errors.New("SplitKeys is nil")
	}
	return nil
}

type DBInfo struct {
	DBName  string
	ColName string
}

func (d *DBInfo) Validate() error {

	if d.DBName == "" {
		return errors.New("DBName is empty")
	}
	if d.ColName == "" {
		return errors.New("ColName is empty")
	}
	return nil
}

type DBFunc func(v map[string]interface{}) (*mongo.Client, string, error) // 外部如何使用用于分库分表的 { key : value, ... }
type ColFunc func(v map[string]interface{}) (string, error)               // 外部如何使用用于分库分表的 { key : value, ... }

type SplitKeys map[string]struct{}

// NewSplitKeys Simulate Set
func NewSplitKeys(keys []string) SplitKeys {
	s := SplitKeys{}
	for _, f := range keys {
		s[f] = struct{}{}
	}
	return s
}

func MongoFindOneAndUpdateOptions(opts ...IMongoOption) []*options.FindOneAndUpdateOptions {
	optArr := make([]*options.FindOneAndUpdateOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*FindOneAndUpdateOptions)
		if i == nil {
			continue
		}
		o := &i.FindOneAndUpdateOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoDeleteOptions(opts ...IMongoOption) []*options.DeleteOptions {
	optArr := make([]*options.DeleteOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*DeleteOptions)
		if i == nil {
			continue
		}
		o := &i.DeleteOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoCountOptions(opts ...IMongoOption) []*options.CountOptions {
	optArr := make([]*options.CountOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*CountOptions)
		if i == nil {
			continue
		}
		o := &i.CountOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoUpdateOptions(opts ...IMongoOption) []*options.UpdateOptions {
	optArr := make([]*options.UpdateOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*UpdateOptions)
		if i == nil {
			continue
		}
		o := &i.UpdateOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoFindOneOptions(opts ...IMongoOption) []*options.FindOneOptions {
	optArr := make([]*options.FindOneOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*FindOneOptions)
		if i == nil {
			continue
		}
		if i.SplitValues != nil {
			continue
		}
		o := &i.FindOneOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoFindOptions(opts ...IMongoOption) []*options.FindOptions {
	optArr := make([]*options.FindOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*FindOptions)
		if i == nil {
			continue
		}
		o := &i.FindOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoFindOneAndDeleteOptions(opts ...IMongoOption) []*options.FindOneAndDeleteOptions {
	optArr := make([]*options.FindOneAndDeleteOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*FindOneAndDeleteOptions)
		if i == nil {
			continue
		}
		o := &i.FindOneAndDeleteOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoInsertOneOptions(opts ...IMongoOption) []*options.InsertOneOptions {
	optArr := make([]*options.InsertOneOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*InsertOneOptions)
		if i == nil {
			continue
		}
		o := &i.InsertOneOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoInsertManyOptions(opts ...IMongoOption) []*options.InsertManyOptions {
	optArr := make([]*options.InsertManyOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*InsertManyOptions)
		if i == nil {
			continue
		}
		o := &i.InsertManyOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoBulkWriteOptions(opts ...IMongoOption) []*options.BulkWriteOptions {
	optArr := make([]*options.BulkWriteOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*BulkWriteOptions)
		if i == nil {
			continue
		}
		o := &i.BulkWriteOptions
		optArr = append(optArr, o)
	}
	return optArr
}

func MongoDistinctOptions(opts ...IMongoOption) []*options.DistinctOptions {
	optArr := make([]*options.DistinctOptions, 0, len(opts))
	for _, item := range opts {
		i := item.(*DistinctOptions)
		if i == nil {
			continue
		}
		o := &i.DistinctOptions
		optArr = append(optArr, o)
	}
	return optArr
}

type DBOption func(*DBOptions)

func DBSplitOption(splitInfo *DBSplitInfo) DBOption {
	return func(imo *DBOptions) {
		imo.DBSplitInfo = splitInfo
	}
}

func DBInfoOption(info *DBInfo) DBOption {
	return func(imo *DBOptions) {
		imo.DBInfo = info
	}
}

func GetDBOptions(opt ...DBOption) DBOptions {
	opts := DBOptions{}

	for _, optFunc := range opt {
		optFunc(&opts)
	}

	return opts
}

func PrimaryPreferred(opts ...IMongoOption) bool {
	for _, item := range opts {
		i, ok := item.(*FindOptions)
		if !ok || i == nil {
			continue
		}
		if i.PrimaryPreferred == nil {
			continue
		}
		return *i.PrimaryPreferred
	}

	for _, item := range opts {
		i, ok := item.(*FindOneOptions)
		if !ok || i == nil {
			continue
		}
		if i.PrimaryPreferred == nil {
			continue
		}
		return *i.PrimaryPreferred
	}

	return false
}
