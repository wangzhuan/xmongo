package v2

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"xgit.xiaoniangao.cn/xngo/lib/xmongo/v2/xoptions"
)

type testMember struct {
	UID    int64  `bson:"uid"`
	Name   string `bson:"name"`
	Age    int    `bson:"age"`
	Delete int8   `bson:"d"`
}

func generateMembers(limit, base int) []testMember {
	max := limit + base
	members := make([]testMember, 0, limit)
	for i := base; i < max; i++ {
		rand.Seed(time.Now().UnixNano())
		m := testMember{
			UID:    int64(i),
			Name:   fmt.Sprintf("name:%d", i),
			Age:    rand.Intn(100),
			Delete: 0,
		}
		members = append(members, m)
	}

	return members
}

func initSharding(client *mongo.Client, dbName, colName string) {
	ctx := context.Background()
	db := client.Database("admin")
	runOpt := options.RunCmd().SetReadPreference(readpref.Primary())

	sr := db.RunCommand(ctx, bson.D{{"enablesharding", dbName}}, runOpt)
	if sr.Err() != nil {
		log.Printf("enablesharding result: %+v", sr)
	}
	shardCollection := fmt.Sprintf("%s.%s", dbName, colName)
	//1 for ranged based sharding
	//"hashed" to specify a hashed shard key.
	//shareKey := bson.M{"_id": 1}
	// https://docs.mongodb.com/v4.2/reference/command/shardCollection/#dbcmd.shardCollection
	shareKey := bson.M{"_id": "hashed"}

	sr = db.RunCommand(ctx, bson.D{
		{"shardcollection", shardCollection},
		{"key", shareKey},
		{"unique", false}, //When true, the unique option ensures that the underlying index enforces a unique constraint. Hashed shard keys do not support unique constraints. Defaults to false.
		{"numInitialChunks", 5},
	}, runOpt)
	if sr.Err() != nil {
		log.Printf("shardcollection result: %+v", sr)
	}
}

//The total size of all the documents array elements must be less than or equal to the maximum BSON document size.
//link: https://docs.mongodb.com/manual/reference/command/insert/
//https://docs.mongodb.com/manual/reference/limits/#mongodb-limit-Write-Command-Batch-Limit-Size
//https://docs.mongodb.com/v4.2/reference/limits/#sessions
//Changed in version 3.6: The limit raises from 1,000 to 100,000 writes.
//100,000 writes are allowed in a single batch operation, defined by a single request to the server.
func insertShardingDocs(cll *Client, limit int) error {
	if limit < 1 {
		return nil
	}
	batchLimit := int(1e5)
	count := limit / batchLimit
	base := 0
	remain := 0
	if count < 1 {
		remain = limit
	} else {
		remain = limit % batchLimit
	}

	for i := 0; i < count; i++ {
		base = i * batchLimit
		fmt.Printf("start index:%d, base：%d\n", i, base)
		err := doInsertShardingDocs(cll, batchLimit, base)
		if err != nil {
			return err
		}
	}
	if remain == 0 {
		return nil
	}
	fmt.Printf("remain: %d\n", remain)
	return doInsertShardingDocs(cll, remain, base)
}

func doInsertShardingDocs(cll *Client, batchLimit, base int) error {
	members := generateMembers(batchLimit, base)
	objs := make([]interface{}, 0, batchLimit)
	for _, item := range members {
		objs = append(objs, item)
	}
	_, err := cll.InsertMany(objs)
	return err
}

func TestNewWithShard(t *testing.T) {
	dbName := "sharding_test_auto_increase"
	colName := "test_member"
	client := shardMongoClient()
	initSharding(client, dbName, colName)
	indexes := client.Database(dbName).Collection(colName).Indexes()
	indexName, e := indexes.CreateOne(context.Background(), mongo.IndexModel{
		Keys: bsonx.Doc{{"uid", bsonx.Int32(1)}},
	})
	assert.NoError(t, e)
	log.Printf("index_name: %s", indexName)

	dbInfo := xoptions.DBInfo{
		DBName:  dbName,
		ColName: colName,
	}
	dbInfoOption := xoptions.DBInfoOption(&dbInfo)
	cl, clErr := NewWith(client, dbInfoOption)
	assert.NoError(t, clErr)
	assert.True(t, cl != nil)

	var members []testMember
	opt := xoptions.FindOptions{}
	opt.FindOptions.SetLimit(2)
	filter := bson.D{}
	err := cl.FindAll(filter, &members, &opt)
	assert.NoError(t, err)

	if len(members) < 1 {
		limit := int(1e6)
		err = insertShardingDocs(cl, limit)
		assert.NoError(t, err)
	}
}
