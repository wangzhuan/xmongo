package v2

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"xgit.xiaoniangao.cn/xngo/lib/xmongo/v2/xoperators"
	"xgit.xiaoniangao.cn/xngo/lib/xmongo/v2/xoptions"
)

type DeleteResult struct {
	mongo.DeleteResult
}

type UpdateResult struct {
	mongo.UpdateResult
}

type InsertOneResult struct {
	mongo.InsertOneResult
}

type InsertManyResult struct {
	mongo.InsertManyResult
}

type Client struct {
	Client *mongo.Client
	Option xoptions.DBOptions
}

func idBson(idParam interface{}) bson.D {
	var id primitive.ObjectID
	if v, ok := idParam.(string); ok {
		id, _ = primitive.ObjectIDFromHex(v)
	}
	return bson.D{{"_id", id}}
}

func NewWith(cl *mongo.Client, opts ...xoptions.DBOption) (*Client, error) {
	opt := xoptions.GetDBOptions(opts...)
	if opt.DBInfo == nil && opt.DBSplitInfo == nil {
		return nil, errors.New("DBInfo and DBSplitInfo is nil")
	}

	if opt.DBInfo != nil {
		err := opt.DBInfo.Validate()
		if err != nil {
			return nil, err
		}
	}
	if opt.DBSplitInfo != nil {
		err := opt.DBSplitInfo.Validate()
		if err != nil {
			return nil, err
		}
	}

	return &Client{
		Client: cl,
		Option: opt,
	}, nil
}

func (client *Client) dbName(splitValues map[string]interface{}) (*mongo.Client, string, error) {
	if len(splitValues) == 0 {
		if client.Option.DBInfo == nil {
			return nil, "", errors.New("DBInfo is nil")
		}
		if client.Client == nil {
			return nil, "", errors.New("client is nil")
		}
		return client.Client, client.Option.DBInfo.DBName, nil
	}

	return client.Option.DBSplitInfo.DBFunc(splitValues)
}

func (client *Client) colName(splitValues map[string]interface{}) (string, error) {
	if len(splitValues) == 0 {
		return client.Option.DBInfo.ColName, nil
	}
	return client.Option.DBSplitInfo.ColFunc(splitValues)
}

func (client *Client) collection(rp *readpref.ReadPref, splitValues map[string]interface{}) (*mongo.Collection, error) {
	d := &options.DatabaseOptions{ReadPreference: rp}
	c, dbName, err := client.dbName(splitValues)
	if err != nil {
		return nil, err
	}
	colName, colErr := client.colName(splitValues)
	if colErr != nil {
		return nil, colErr
	}
	return c.Database(dbName, d).Collection(colName), nil
}

func (client *Client) SecondaryColl(splitValues map[string]interface{}, opts ...xoptions.IMongoOption) (*mongo.Collection, error) {
	rp := readpref.SecondaryPreferred()
	if xoptions.PrimaryPreferred(opts...) {
		rp = readpref.PrimaryPreferred()
	}
	return client.collection(rp, splitValues)
}

func (client *Client) PrimaryColl(splitValues map[string]interface{}) (*mongo.Collection, error) {
	rp := readpref.PrimaryPreferred()
	return client.collection(rp, splitValues)
}

func (client *Client) Count(query interface{}, opts ...xoptions.IMongoOption) (int64, error) {
	mongoOpts := xoptions.MongoCountOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.SecondaryColl(splitValues)
	if err != nil {
		return 0, err
	}
	return c.CountDocuments(ctx, query, mongoOpts...)
}

func (client *Client) FindId(id interface{}, result interface{}, opts ...xoptions.IMongoOption) error {
	mongoOpts := xoptions.MongoFindOneOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.SecondaryColl(splitValues)
	if err != nil {
		return err
	}
	filter := idBson(id)

	return c.FindOne(ctx, filter, mongoOpts...).Decode(result)
}

func (client *Client) FindOne(query interface{}, result interface{}, opts ...xoptions.IMongoOption) error {
	mongoOpts := xoptions.MongoFindOneOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.SecondaryColl(splitValues)
	if err != nil {
		return err
	}

	return c.FindOne(ctx, query, mongoOpts...).Decode(result)
}

func (client *Client) FindAll(query interface{}, results interface{}, opts ...xoptions.IMongoOption) error {
	mongoOpts := xoptions.MongoFindOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.SecondaryColl(splitValues)
	if err != nil {
		return err
	}

	cur, err := c.Find(ctx, query, mongoOpts...)
	if err != nil {
		return err
	}

	err = cur.All(ctx, results)
	return err
}

func (client *Client) Insert(docs interface{}, opts ...xoptions.IMongoOption) (*InsertOneResult, error) {
	mongoOpts := xoptions.MongoInsertOneOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}
	return insertResult(c.InsertOne(ctx, docs, mongoOpts...))
}

func (client *Client) InsertMany(documents []interface{}, opts ...xoptions.IMongoOption) (*InsertManyResult, error) {
	mongoOpts := xoptions.MongoInsertManyOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}
	return insertManyResult(c.InsertMany(ctx, documents, mongoOpts...))
}

func (client *Client) UpdateOne(filter interface{}, update interface{}, opts ...xoptions.IMongoOption) (*UpdateResult, error) {
	mongoOpts := xoptions.MongoUpdateOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}
	return updateResult(c.UpdateOne(ctx, filter, update, mongoOpts...))
}

func (client *Client) UpdateByID(id interface{}, update interface{}, opts ...xoptions.IMongoOption) (*UpdateResult, error) {
	mongoOpts := xoptions.MongoUpdateOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}
	return updateResult(c.UpdateByID(ctx, id, update, mongoOpts...))
}

func (client *Client) UpdateMany(filter interface{}, update interface{}, opts ...xoptions.IMongoOption) (*UpdateResult, error) {
	mongoOpts := xoptions.MongoUpdateOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}
	return updateResult(c.UpdateMany(ctx, filter, update, mongoOpts...))
}

func (client *Client) UpdateSelective(filter interface{}, update interface{}, opts ...xoptions.IMongoOption) (*UpdateResult, error) {
	mongoOpts := xoptions.MongoUpdateOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}

	return updateResult(c.UpdateOne(ctx, filter, bson.M{xoperators.Set: update}, mongoOpts...))
}

func (client *Client) UpdateIdSelective(id interface{}, update interface{}, opts ...xoptions.IMongoOption) (*UpdateResult, error) {
	mongoOpts := xoptions.MongoUpdateOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}

	return updateResult(c.UpdateByID(ctx, id, bson.M{xoperators.Set: update}, mongoOpts...))
}

func (client *Client) UpdateAllSelective(filter interface{}, update interface{}, opts ...xoptions.IMongoOption) (*UpdateResult, error) {
	mongoOpts := xoptions.MongoUpdateOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}

	return updateResult(c.UpdateMany(ctx, filter, bson.M{xoperators.Set: update}, mongoOpts...))
}

func (client *Client) Upsert(filter interface{}, update interface{}, opts ...xoptions.IMongoOption) (*UpdateResult, error) {
	mongoOpts := xoptions.MongoUpdateOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}
	upsertOpt := options.Update().SetUpsert(true)
	mongoOpts = append(mongoOpts, upsertOpt)
	return updateResult(c.UpdateOne(ctx, filter, update, mongoOpts...))
}

func (client *Client) UpsertId(id interface{}, update interface{}, opts ...xoptions.IMongoOption) (*UpdateResult, error) {
	mongoOpts := xoptions.MongoUpdateOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}

	upsertOpt := options.Update().SetUpsert(true)
	mongoOpts = append(mongoOpts, upsertOpt)
	return updateResult(c.UpdateByID(ctx, id, update, mongoOpts...))
}

func (client *Client) DeleteOne(filter interface{}, opts ...xoptions.IMongoOption) (*DeleteResult, error) {
	mongoOpts := xoptions.MongoDeleteOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}
	return delResult(c.DeleteOne(ctx, filter, mongoOpts...))
}

func (client *Client) RemoveId(id interface{}, opts ...xoptions.IMongoOption) (*DeleteResult, error) {
	mongoOpts := xoptions.MongoDeleteOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}
	filter := idBson(id)
	return delResult(c.DeleteOne(ctx, filter, mongoOpts...))
}

func (client *Client) RemoveAll(filter interface{}, opts ...xoptions.IMongoOption) (*DeleteResult, error) {
	mongoOpts := xoptions.MongoDeleteOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}
	return delResult(c.DeleteMany(ctx, filter, mongoOpts...))
}

func (client *Client) ExistById(id interface{}, opts ...xoptions.IMongoOption) (bool, error) {
	mongoOpts := xoptions.MongoCountOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.SecondaryColl(splitValues)
	if err != nil {
		return false, err
	}
	filter := idBson(id)
	ct, err := c.CountDocuments(ctx, filter, mongoOpts...)
	if err != nil {
		return false, err
	}
	return ct > 0, nil
}

func (client *Client) ExistByQuery(filter interface{}, opts ...xoptions.IMongoOption) (bool, error) {
	mongoOpts := xoptions.MongoCountOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.SecondaryColl(splitValues)
	if err != nil {
		return false, err
	}
	ct, err := c.CountDocuments(ctx, filter, mongoOpts...)
	if err != nil {
		return false, err
	}
	return ct > 0, nil
}

func (client *Client) FindOneAndUpdate(filter interface{}, update interface{}, result interface{}, opts ...xoptions.IMongoOption) error {
	mongoOpts := xoptions.MongoFindOneAndUpdateOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return err
	}

	err = c.FindOneAndUpdate(ctx, filter, update, mongoOpts...).Decode(result)
	return err
}

func (client *Client) FindOneAndDelete(filter interface{}, result interface{}, opts ...xoptions.IMongoOption) error {
	mongoOpts := xoptions.MongoFindOneAndDeleteOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return err
	}

	return c.FindOneAndDelete(ctx, filter, mongoOpts...).Decode(result)
}

func delResult(rt *mongo.DeleteResult, err error) (*DeleteResult, error) {
	if err != nil {
		return nil, err
	}
	if rt == nil {
		return nil, errors.New("del result is nil")
	}
	dResult := &DeleteResult{DeleteResult: *rt}
	return dResult, err
}

func insertResult(rt *mongo.InsertOneResult, err error) (*InsertOneResult, error) {
	if err != nil {
		return nil, err
	}
	if rt == nil {
		return nil, errors.New("InsertOneResult is nil")
	}
	dResult := &InsertOneResult{InsertOneResult: *rt}
	return dResult, err
}

func insertManyResult(rt *mongo.InsertManyResult, err error) (*InsertManyResult, error) {
	if err != nil {
		return nil, err
	}
	if rt == nil {
		return nil, errors.New("InsertManyResult is nil")
	}
	dResult := &InsertManyResult{InsertManyResult: *rt}
	return dResult, err
}

func updateResult(ur *mongo.UpdateResult, err error) (*UpdateResult, error) {
	if err != nil {
		return nil, err
	}
	if ur == nil {
		return nil, errors.New("update result is nil")
	}
	upResult := &UpdateResult{UpdateResult: *ur}
	return upResult, err
}

func (client *Client) DistinctFind(field string, query interface{}, opts ...xoptions.IMongoOption) ([]interface{}, error) {
	mongoOpts := xoptions.MongoDistinctOptions(opts...)
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.SecondaryColl(splitValues)
	if err != nil {
		return nil, err
	}

	results, findErr := c.Distinct(ctx, field, query, mongoOpts...)
	if err != nil {
		return results, findErr
	}

	return results, nil
}

func (client *Client) Aggregate(pipeline interface{}, result interface{}, opts ...xoptions.IMongoOption) error {
	ctx := context.Background()
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.SecondaryColl(splitValues)
	if err != nil {
		return err
	}

	cursor, aggrErr := c.Aggregate(ctx, pipeline)
	if aggrErr != nil {
		return aggrErr
	}
	return cursor.All(context.TODO(), result)
}

func (client *Client) BulkWrite(ctx context.Context, models []mongo.WriteModel, opts ...xoptions.IMongoOption) (*mongo.BulkWriteResult, error) {
	mongoOpts := xoptions.MongoBulkWriteOptions(opts...)
	splitValues := xoptions.GetSplitValues(opts...)
	c, err := client.PrimaryColl(splitValues)
	if err != nil {
		return nil, err
	}

	return c.BulkWrite(ctx, models, mongoOpts...)
}
