package v2

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"xgit.xiaoniangao.cn/xngo/lib/xmongo/v2/xoperators"
	"xgit.xiaoniangao.cn/xngo/lib/xmongo/v2/xoptions"
)

func mongoClient() *mongo.Client {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://192.168.12.23:27077/admin?minPoolSize=10&maxIdleTimeMS=10000"))
	if err != nil {
		log.Fatalf("mongo.Connect err: %v", err)
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatalf("mongo ping err: %v", err)
	}
	return client
}

func shardMongoClient() *mongo.Client {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://root:123456@192.168.22.47:27000,192.168.22.48:27000,192.168.22.49:27000/?authSource=admin&minPoolSize=10&maxIdleTimeMS=10000"))
	if err != nil {
		log.Fatalf("mongo.Connect err: %v", err)
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatalf("mongo ping err: %v", err)
	}
	return client
}

func TestNewWithStandAlone(t *testing.T) {
	dbName := "stand_test"
	colName := "test_member"
	client := mongoClient()

	dbInfo := xoptions.DBInfo{
		DBName:  dbName,
		ColName: colName,
	}
	dbInfoOption := xoptions.DBInfoOption(&dbInfo)
	cl, clErr := NewWith(client, dbInfoOption)
	assert.NoError(t, clErr)
	assert.True(t, cl != nil)

	var members []testMember
	opt := xoptions.FindOptions{}
	opt.FindOptions.SetLimit(2)
	filter := bson.D{}
	err := cl.FindAll(filter, &members, &opt)
	assert.NoError(t, err)
	if len(members) < 1 {
		limit := 1200
		base := 0
		members = generateMembers(limit, base)
		objs := make([]interface{}, 0, limit)
		for _, item := range members {
			objs = append(objs, item)
		}
		_, err = cl.InsertMany(objs)
		assert.NoError(t, err)
	}

	var results []testMember
	opt.FindOptions.SetLimit(5).SetSort(bson.D{{"_id", -1}})
	filter = bson.D{
		{"age", bson.M{xoperators.LTE: 30}},
	}
	err = cl.FindAll(filter, &results, &opt)
	assert.NoError(t, err)
	for _, item := range results {
		t.Logf("%+v\n", item)
	}
}

const groupUserDBNum = 2
const groupUserColNum = 2

func TestWithSplit(t *testing.T) {
	client := mongoClient()
	splitInfo := xoptions.DBSplitInfo{
		SplitKeys: xoptions.NewSplitKeys([]string{"id"}),
		DBFunc: func(v map[string]interface{}) (*mongo.Client, string, error) {
			mid := v["id"].(int64)
			id := int(mid % groupUserDBNum)
			dbName := fmt.Sprintf("tia_shutterbugs_user_%d", id)
			return client, dbName, nil
		},
		ColFunc: func(v map[string]interface{}) (string, error) {
			mid := v["id"].(int64)
			name := fmt.Sprintf("user_%d", (mid/groupUserDBNum)%groupUserColNum)
			return name, nil
		},
	}
	dbInfoOption := xoptions.DBSplitOption(&splitInfo)
	cl, clErr := NewWith(client, dbInfoOption)
	assert.NoError(t, clErr)
	assert.True(t, cl != nil)
	mid := int64(51512)
	filter := bson.D{{"mid", mid}}
	var dObj userInfo
	opt := xoptions.FindOneOptions{SplitValues: bson.M{"id": mid}}
	err := cl.FindOne(filter, &dObj, &opt)
	if err == mongo.ErrNoDocuments {
		t.Log("err == mongo.ErrNoDocuments")
		return
	}
	assert.NoError(t, err)
	t.Logf("obj: %+v", dObj)
}

type userInfo struct {
	Mid  int64  `bson:"mid"`
	Name string `bson:"name"`
	Uid  string `bson:"_id"`
	ID   int64  `bson:"id"`
}

func TestWithSplit2(t *testing.T) {
	client := mongoClient()
	splitInfo := xoptions.DBSplitInfo{
		SplitKeys: xoptions.NewSplitKeys([]string{"id"}),
		DBFunc: func(v map[string]interface{}) (*mongo.Client, string, error) {
			mid := v["id"].(int64)
			id := int(mid % groupUserDBNum)
			dbName := fmt.Sprintf("tia_shutterbugs_user_%d", id)
			return client, dbName, nil
		},
		ColFunc: func(v map[string]interface{}) (string, error) {
			mid := v["id"].(int64)
			name := fmt.Sprintf("user_%d", (mid/groupUserDBNum)%groupUserColNum)
			return name, nil
		},
	}
	dbInfoOption := xoptions.DBSplitOption(&splitInfo)
	cl, clErr := NewWith(client, dbInfoOption)
	assert.NoError(t, clErr)
	assert.True(t, cl != nil)
	mid := int64(51512)
	filter := bson.D{}
	users := make([]userInfo, 0)

	opt := xoptions.FindOptions{SplitValues: bson.M{"id": mid}}
	opt.FindOptions.SetLimit(10).SetSort(bson.D{{"id", -1}})
	err := cl.FindAll(filter, &users, &opt)
	if err == mongo.ErrNoDocuments {
		t.Log("err == mongo.ErrNoDocuments")
		return
	}
	assert.NoError(t, err)
	for index, item := range users {
		t.Logf("index: %d user: %+v", index, item)
	}

}
