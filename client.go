package xmongo

import (
	"errors"

	mgo "gitlab.com/wangzhuan/github.com.globalsign.mgo"
	"gitlab.com/wangzhuan/github.com.globalsign.mgo/bson"
)

type Client struct {
	Session *mgo.Session // 外部传入，不需要和 xconf 耦合
	DBName  string
	ColName string
}

func NewClient(session *mgo.Session, dbName string, colName string) (client *Client, err error) {
	// session有效性
	if session == nil {
		return nil, errors.New("MongoDB Session is nil")
	}
	// 检查库名，表名
	if dbName == "" || colName == "" {
		return nil, errors.New("DBName or ColName of config is empty")
	}
	return &Client{
		session,
		dbName,
		colName,
	}, nil
}

func (client *Client) GetSessionAndCollection() (s *mgo.Session, c *mgo.Collection) {
	s = client.Session.Copy()
	c = s.DB(client.DBName).C(client.ColName)
	return
}

func (client *Client) Count(query interface{}) (n int, err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.Find(query).Count()
}

func (client *Client) CountWithSelect(query interface{}) (n int, err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.Find(query).Select(query).Count()
}

func (client *Client) FindId(id interface{}, result interface{}) (err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.FindId(id).One(result)
}

func (client *Client) FindOne(query interface{}, result interface{}, extraOps func(*mgo.Query) *mgo.Query) (err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	if extraOps != nil {
		return extraOps(c.Find(query)).One(result)
	}
	return c.Find(query).One(result)
}

func (client *Client) FindAll(query interface{}, results interface{}, extraOps func(q *mgo.Query) *mgo.Query) (err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	if extraOps != nil {
		return extraOps(c.Find(query)).All(results)
	}
	return c.Find(query).All(results)
}

func (client *Client) Insert(docs interface{}) (err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.Insert(docs)
}

func (client *Client) Update(selector interface{}, update interface{}) (err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.Update(selector, update)
}

func (client *Client) UpdateId(id interface{}, update interface{}) error {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.UpdateId(id, update)
}

func (client *Client) UpdateAll(selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.UpdateAll(selector, update)
}

func (client *Client) UpdateSelective(selector interface{}, update interface{}) (err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.Update(selector, bson.M{"$set": update})
}

func (client *Client) UpdateIdSelective(id interface{}, update interface{}) error {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.UpdateId(id, bson.M{"$set": update})
}

func (client *Client) UpdateAllSelective(selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.UpdateAll(selector, bson.M{"$set": update})
}

func (client *Client) Upsert(selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.Upsert(selector, update)
}

func (client *Client) UpsertId(id interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.UpsertId(id, update)
}

func (client *Client) Remove(selector interface{}) error {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.Remove(selector)
}

func (client *Client) RemoveId(id interface{}) error {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.RemoveId(id)
}

func (client *Client) RemoveAll(selector interface{}) (info *mgo.ChangeInfo, err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.RemoveAll(selector)
}

func (client *Client) ExistById(id interface{}) (bool, error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	count, err := c.FindId(id).Count()
	return ParseExistResult(count, err)
}

func (client *Client) ExistByQuery(selector interface{}) (bool, error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	count, err := c.Find(selector).Count()
	return ParseExistResult(count, err)
}

func ParseExistResult(count int, err error) (bool, error) {
	if err == mgo.ErrNotFound {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	if count > 0 {
		return true, nil
	}
	return false, nil
}

func (client *Client) Apply(query interface{}, change mgo.Change, result interface{}) (info *mgo.ChangeInfo, err error) {
	s, c := client.GetSessionAndCollection()
	defer s.Close()
	return c.Find(query).Apply(change, result)
}
