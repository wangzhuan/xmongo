package xmongo

import (
	"time"

	"gitlab.com/wangzhuan/github.com.globalsign.mgo/bson"
)

func (client *Client) Create(model Model) error {
	if err := callToBeforeCreateHooks(model); err != nil {
		return err
	}
	model.SetID(nil)
	if err := client.Insert(model); err != nil {
		return err
	}
	if err := callToAfterCreateHooks(model); err != nil {
		return err
	}

	return nil
}

func (client *Client) Delete(model Model) error {
	if err := callToBeforeDeleteHooks(model); err != nil {
		return err
	}
	if err := client.Update(bson.M{ID: model.GetID()}, bson.M{"$set": model}); err != nil {
		return err
	}
	if err := callToAfterDeleteHooks(model); err != nil {
		return err
	}
	return nil
}

// 更新model updated_at
func (client *Client) UpdateM(model Model) error {
	if err := callToBeforeUpdateHooks(model); err != nil {
		return err
	}

	if err := client.Update(bson.M{ID: model.GetID()}, bson.M{"$set": model}); err != nil {
		return err
	}
	if err := callToAfterUpdateHooks(model); err != nil {
		return err
	}

	return nil
}

// 更新updated_at
func (client *Client) UpdateT(selector interface{}, update interface{}) error {
	if updateM, ok := update.(bson.M); ok {
		if set, ok := updateM[Set]; ok {
			if m, ok := set.(bson.M); ok {
				m[UpdatedAt] = time.Now()
				update = updateM
			}
		}
	}
	if err := client.Update(selector, update); err != nil {
		return err
	}
	// todo 转换model

	return nil
}

// todo FindById
// todo First
// todo aggregate
// todo 更换driver
