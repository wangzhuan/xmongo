package xmongo

type Model interface {
	// 转换string to bson objectId
	PrepareID(id interface{}) (interface{}, error)

	GetID() interface{}
	SetID(id interface{})
}
