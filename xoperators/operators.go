package xoperators

// 参考文档：
// https://docs.mongodb.com/manual/reference/operator/query-comparison/

// Evaluation Query Operators
const (
	Expr       = "$expr"       // Allows use of aggregation expressions within the query language.
	JsonSchema = "$jsonSchema" // Validate documents against the given JSON Schema.
	Mod        = "$mod"        // Performs a modulo operation on the value of a field and selects documents with a specified result.
	Regex      = "$regex"      // Selects documents where values match a specified regular expression.
	Text       = "$text"       // Performs text search.
	Where      = "$where"      // Matches documents that satisfy a JavaScript expression.
)

// Comparison Query Operators0
const (
	EQ  = "$eq"  // Matches values that are equal to a specified value.
	GT  = "$gt"  // Matches values that are greater than a specified value.
	GTE = "$gte" // Matches values that are greater than or equal to a specified value.
	IN  = "$in"  // Matches any of the values specified in an array.
	LT  = "$lt"  // Matches values that are less than a specified value.
	LTE = "$lte" // Matches values that are less than or equal to a specified value.
	NE  = "$ne"  // Matches all values that are not equal to a specified value.
	NIN = "$nin" // Matches none of the values specified in an array.
)

// Logical Query Operators
const (
	AND = "$and" // Joins query clauses with a logical AND returns all documents that match the conditions of both clauses.
	NOT = "$not" // Inverts the effect of a query expression and returns documents that do not match the query expression.
	NOR = "$nor" // Joins query clauses with a logical NOR returns all documents that fail to match both clauses.
	OR  = "$or"  // Joins query clauses with a logical OR returns all documents that match the conditions of either clause.
)

// Element Query Operators
const (
	Exists = "$exists" // Matches documents that have the specified field.
	Type   = "$type"   // Selects documents if a field is of the specified type.
)

// Array Query Operators
const (
	All       = "$all"       // Matches arrays that contain all elements specified in the query.
	ElemMatch = "$elemMatch" // Selects documents if element in the array field matches all the specified $elemMatch conditions.
	Size      = "$size"      // Selects documents if the array field is a specified size.
)

// Bitwise Query Operators
const (
	BitsAllClear = "$bitsAllClear" // Matches numeric or binary values in which a set of bit positions all have a value of 0.
	BitsAllSet   = "$bitsAllSet"   // Matches numeric or binary values in which a set of bit positions all have a value of 1.
	BitsAnyClear = "$bitsAnyClear" // Matches numeric or binary values in which any bit from a set of bit positions has a value of 0.
	BitsAnySet   = "$bitsAnySet"   // Matches numeric or binary values in which any bit from a set of bit positions has a value of 1.
)

// Miscellaneous Query Operators
const (
	Comment = "$comment" // Adds a comment to a query predicate.
	Rand    = "$rand"    // Generates a random float between 0 and 1. New in version 4.4.2.
)

// Update Operators
const (
	CurrentDate = "$currentDate" // Sets the value of a field to current date, either as a Date or a Timestamp.
	INC         = "$inc"         //Increments the value of the field by the specified amount.
	MIN         = "$min"         // Only updates the field if the specified value is less than the existing field value.
	MAX         = "$max"         // Only updates the field if the specified value is greater than the existing field value.
	MUL         = "$mul"         // Multiplies the value of the field by the specified amount.
	Rename      = "$rename"      // Renames a field.
	Set         = "$set"         // Sets the value of a field in a document.
	SetOnInsert = "$setOnInsert" // Sets the value of a field if an update results in an insert of a document. Has no effect on update operations that modify existing documents.
	Unset       = "$unset"       // Removes the specified field from a document.

	// Array Update

	AddToSet = "$addToSet" // Adds elements to an array only if they do not already exist in the set.
	Pop      = "$pop"      // Removes the first or last item of an array.
	Pull     = "$pull"     // Removes all array elements that match a specified query.
	Push     = "$push"     // Adds an item to an array.
	PullAll  = "$pullAll"  // Removes all matching values from an array.

	Each     = "$each"     // Modifies the $push and $addToSet operators to append multiple items for array updates.
	Position = "$position" // Modifies the $push operator to specify the position in the array to add elements.
	Slice    = "$slice"    // Modifies the $push operator to limit the size of updated arrays.
	Sort     = "$sort"     // Modifies the $push operator to reorder documents stored in an array.

	Bit = "$bit" // Performs bitwise AND, OR, and XOR updates of integer values.
)
