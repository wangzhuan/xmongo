### Usage

#### 不分库分表

```go
package main
import "xgit.xiaoniangao.cn/xngo/lib/xmongo"
import "xgit.xiaoniangao.cn/xngo/service/xgin_api"
import "xgit.xiaoniangao.cn/xngo/service/xgin/conf"

func main() {
    c, err := NewClient(conf.DBS["skel"], "test_xmongo_db", "test_xmongo_col")
    if err != nil {
        log.Fatal(err)
    }
    m := Model{
        Name: "hello",
        ID: 12000,
    }
    m0 := xgin_api.Skel{
        Name: "hello",
        ID: 12001,
    }
    err = c.Insert(m)
    log.Println( "Insert:", m, err )
    
    err = c.Insert(m0)
    log.Println( "Insert:", m0, err )
    
    n, err := c.Count(nil)
    log.Println( "Count:", n, err )
    
    ms := []xgin_api.Skel{}
    err = c.FindAll(nil, &ms, nil )
    log.Println("findAll:", ms, err)
    
    m1 := xgin_api.Skel{}
    err = c.FindOne( bson.M{"name":"hello"}, &m1, nil)
    log.Println("findOne:", m1, err)
    
    m2 := xgin_api.Skel{}
    err = c.FindOne( bson.M{"name":"hello"}, &m2, func(query *mgo.Query) *mgo.Query {
        return query.Skip(1).Limit(10)
    })
    log.Println("findOne skip one:", m2, err)
    
    m3 := xgin_api.Skel{}
    err = c.FindOne( bson.M{"name":"hello"}, &m2, func(query *mgo.Query) *mgo.Query {
        return query.Skip(1).Limit(10).Sort("-_id")
    })
    log.Println("findOne skip one order by id:", m3, err)
    
    err = c.UpdateId(12000, bson.M{"$set":bson.M{"name":"helloUp"}} )
    var m4 xgin_api.Skel
    err = c.FindOne(bson.M{"_id":12000}, &m4, nil)
    log.Println( "updateId:", m4, err)
    
    _, err = c.UpdateAll(nil, bson.M{"$set":bson.M{"name":"helloUpAll"}} )
    var m5 []xgin_api.Skel
    err = c.FindAll(nil, &m5, nil)
    log.Println( "updateAll:", m5, err)
    
    err = c.RemoveId(12000)
    n, err = c.Count(nil)
    log.Println( "removeId:", n )
    
    _, err = c.RemoveAll(nil)
    n, err =c.Count(nil)
    log.Println( "removeAll:", n)
}
```

#### 分库分表

```go
package main
import "xgit.xiaoniangao.cn/xngo/lib/xmongo"
import "xgit.xiaoniangao.cn/xngo/service/xgin_api"
import "xgit.xiaoniangao.cn/xngo/service/xgin/conf"

func main(){
	c, err := NewSplitClient(conf.DBS["invalid"], []string{"id", "time"}, func(v map[string]interface{}) string {
		id := v["id"].(int)
		return fmt.Sprintf("test_xmongo_db_%d", id%64)
	}, func(v map[string]interface{}) string {
		id := v["id"].(int)
		return fmt.Sprintf("test_xmongo_col_%d", id%64)
	})
	if err != nil {
		log.Fatal(err) // client_test.go:197: DBFunc or ColFun of SplitConfig is empty
	}
	m := xgin_api.Skel{
		Name: "hello",
		ID:   12000,
	}
	m0 := xgin_api.Skel{
		Name: "hello",
		ID:   12001,
	}
	err = c.Insert(map[string]interface{}{
		"id": m.ID,
	}, m)
	log.Println("Insert:", m, err)

	err = c.Insert(map[string]interface{}{
		"id": m0.ID,
	}, m0)
	log.Println("Insert:", m0, err)

	n, err := c.Count(map[string]interface{}{
		"id": m0.ID,
	}, nil)
	log.Println("Count:", n, err)

	ms := []xgin_api.Skel{}
	err = c.FindAll(map[string]interface{}{
		"id": m0.ID,
	}, nil, &ms, nil)
	log.Println("findAll:", ms, err)

	m1 := xgin_api.Skel{}
	err = c.FindOne(map[string]interface{}{
		"id": m0.ID,
	}, bson.M{"name": "hello"}, &m1, nil)
	log.Println("findOne:", m1, err)

	m2 := xgin_api.Skel{}
	err = c.FindOne(map[string]interface{}{
		"id": m0.ID,
	}, bson.M{"name": "hello"}, &m2, func(query *mgo.Query) *mgo.Query {
		return query.Skip(1).Limit(10)
	})
	log.Println("findOne skip one:", m2, err)

	m3 := xgin_api.Skel{}
	err = c.FindOne(map[string]interface{}{
		"id": 0,
	}, bson.M{"name": "hello"}, &m2, func(query *mgo.Query) *mgo.Query {
		return query.Skip(1).Limit(10).Sort("-_id")
	})
	log.Println("findOne skip one order by id:", m3, err)

	err = c.UpdateId(map[string]interface{}{
		"id": 12000,
	}, 12000, bson.M{"$set": bson.M{"name": "helloUp"}})
	var m4 xgin_api.Skel
	err = c.FindOne(map[string]interface{}{
		"id": 12000,
	}, bson.M{"_id": 12000}, &m4, nil)
	log.Println("updateId:", m4, err)

	_, err = c.UpdateAll(map[string]interface{}{
		"id": 12000,
	}, nil, bson.M{"$set": bson.M{"name": "helloUpAll"}})
	var m5 []xgin_api.Skel
	err = c.FindAll(map[string]interface{}{
		"id": 12000,
	}, nil, &m5, nil)
	log.Println("updateAll:", m5, err)

	err = c.RemoveId(map[string]interface{}{
		"id": 12000,
	}, 12000)
	n, err = c.Count(map[string]interface{}{
		"id": 12000,
	}, nil)
	log.Println("removeId:", n)

	_, err = c.RemoveAll(map[string]interface{}{
		"id": 12000,
	}, nil)
	n, err = c.Count(map[string]interface{}{
		"id": 12000,
	}, nil)
	log.Println("removeAll:", n)
}
```