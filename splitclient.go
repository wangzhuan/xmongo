package xmongo

import (
	"errors"
	"fmt"

	mgo "gitlab.com/wangzhuan/github.com.globalsign.mgo"
	"gitlab.com/wangzhuan/github.com.globalsign.mgo/bson"
)

type SplitKeys map[string]struct{}

// Simulate Set
func NewSplitKeys(keys []string) SplitKeys {
	s := SplitKeys{}
	for _, f := range keys {
		s[f] = struct{}{}
	}
	return s
}

type DBFunc func(v map[string]interface{}) (*mgo.Session, string, error) // 外部如何使用用于分库分表的 { key : value, ... }
type ColFunc func(v map[string]interface{}) (string, error)              // 外部如何使用用于分库分表的 { key : value, ... }

type SplitClient struct {
	SplitKeys SplitKeys // 需要的分库分表字段
	DBFunc    DBFunc    // 外部如何使用用于分库分表的 { key : value, ... }
	ColFunc   ColFunc   // 外部如何使用用于分库分表的 { key : value, ... }
}

func NewSplitClient(splitKeys []string, dBFunc DBFunc, colFunc ColFunc) (client *SplitClient, err error) {
	if len(splitKeys) == 0 { // 没有约定分表所用的字段
		return nil, errors.New("splitKeys is empty")
	}
	if dBFunc == nil || colFunc == nil {
		return nil, errors.New("DBFunc or ColFun of SplitConfig is empty")
	}
	return &SplitClient{
		SplitKeys: NewSplitKeys(splitKeys),
		DBFunc:    dBFunc,
		ColFunc:   colFunc,
	}, nil
}

func (client *SplitClient) DB(splitValues map[string]interface{}) (session *mgo.Session, dbName string, err error) {
	// 验证分库分表字段的有效性
	for k := range client.SplitKeys {
		if _, ok := splitValues[k]; !ok {
			err = errors.New(fmt.Sprintf("Missing the Value of SplitKeys [%s]", k))
			return
		}
	}
	session, dbName, err = client.DBFunc(splitValues)
	if err != nil {
		return
	}

	if dbName == "" {
		err = errors.New("Empty DB Name")
		return
	}
	if session == nil {
		err = errors.New("Empty mgo session")
		return
	}
	return
}

func (client *SplitClient) Collection(splitValues map[string]interface{}) (colName string, err error) {
	// 验证分库分表字段的有效性
	for k := range client.SplitKeys {
		if _, ok := splitValues[k]; !ok {
			return "", errors.New(fmt.Sprintf("Missing the Value of SplitKeys [%s]", k))
		}
	}
	colName, err = client.ColFunc(splitValues)
	if err != nil {
		return
	}
	if colName == "" {
		err = errors.New("Empty Collection Name")
		return
	}
	return
}

func (client *SplitClient) GetSessionAndCollection(splitValues map[string]interface{}) (s *mgo.Session, c *mgo.Collection, err error) {
	session, dbName, err := client.DB(splitValues)
	if err != nil {
		return
	}
	colName, err := client.Collection(splitValues)
	if err != nil {
		return
	}
	s = session.Copy()
	c = s.DB(dbName).C(colName)
	return
}

func (client *SplitClient) Count(splitValues map[string]interface{}, query interface{}) (n int, err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.Find(query).Count()
}

func (client *SplitClient) CountWithSelect(splitValues map[string]interface{}, query interface{}) (n int, err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.Find(query).Select(query).Count()
}

func (client *SplitClient) FindId(splitValues map[string]interface{}, id interface{}, result interface{}) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.FindId(id).One(result)
}

func (client *SplitClient) FindOne(splitValues map[string]interface{}, query interface{}, result interface{}, extraOps func(*mgo.Query) *mgo.Query) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	if extraOps != nil {
		return extraOps(c.Find(query)).One(result)
	}
	return c.Find(query).One(result)
}

func (client *SplitClient) FindAll(splitValues map[string]interface{}, query interface{}, results interface{}, extraOps func(q *mgo.Query) *mgo.Query) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	if extraOps != nil {
		return extraOps(c.Find(query)).All(results)
	}
	return c.Find(query).All(results)
}

func (client *SplitClient) Insert(splitValues map[string]interface{}, docs interface{}) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.Insert(docs)
}

func (client *SplitClient) Update(splitValues map[string]interface{}, selector interface{}, update interface{}) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.Update(selector, update)
}

func (client *SplitClient) UpdateId(splitValues map[string]interface{}, id interface{}, update interface{}) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.UpdateId(id, update)
}

func (client *SplitClient) UpdateAll(splitValues map[string]interface{}, selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.UpdateAll(selector, update)
}

func (client *SplitClient) UpdateSelective(splitValues map[string]interface{}, selector interface{}, update interface{}) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.Update(selector, bson.M{"$set": update})
}

func (client *SplitClient) UpdateIdSelective(splitValues map[string]interface{}, id interface{}, update interface{}) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.UpdateId(id, bson.M{"$set": update})
}

func (client *SplitClient) UpdateAllSelective(splitValues map[string]interface{}, selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.UpdateAll(selector, bson.M{"$set": update})
}

func (client *SplitClient) Upsert(splitValues map[string]interface{}, selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.Upsert(selector, update)
}

func (client *SplitClient) UpsertId(splitValues map[string]interface{}, id interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.UpsertId(id, update)
}

func (client *SplitClient) Remove(splitValues map[string]interface{}, selector interface{}) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.Remove(selector)
}

func (client *SplitClient) RemoveId(splitValues map[string]interface{}, id interface{}) (err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.RemoveId(id)
}

func (client *SplitClient) RemoveAll(splitValues map[string]interface{}, selector interface{}) (info *mgo.ChangeInfo, err error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return
	}
	defer s.Close()
	return c.RemoveAll(selector)
}

func (client *SplitClient) ExistById(splitValues map[string]interface{}, id interface{}) (bool, error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return false, err
	}
	defer s.Close()
	count, err := c.FindId(id).Count()
	return ParseExistResult(count, err)
}

func (client *SplitClient) ExistByQuery(splitValues map[string]interface{}, selector interface{}) (bool, error) {
	s, c, err := client.GetSessionAndCollection(splitValues)
	if err != nil {
		return false, err
	}
	defer s.Close()
	count, err := c.Find(selector).Count()
	return ParseExistResult(count, err)
}
